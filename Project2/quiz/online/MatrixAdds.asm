.data
	data: .word 0:32
	array: .word 0:32
	result: .word 0:32
	space: .asciiz "a"
	enter: .asciiz "b"
.text
	
input:
	li $v0, 5
	syscall
	move $t0, $v0   #m
	
	li $v0, 5
	syscall
	move $t1, $v0   #n
	
#the first matrix	
	li $s0, 0
	li $s1, 0
loop1:
	mult $s0, $t1
	mflo $s2
	addu $s2, $s2, $s1
	sll $s2, $s2, 2
	
	li $v0, 5
	syscall
	
	sw $v0, data($s2)		
	
	addu $s1, $s1, 1
	bne $s1, $t1, loop1
	
	move $s1, $zero
	addu $s0, $s0, 1
	bne $s0, $t0, loop1
	
	
#the second maatrix
	li $s0, 0
	li $s1, 0
loop2:
	mult $s0, $t1
	mflo $s2
	addu $s2, $s2, $s1
	sll $s2, $s2, 2
	
	li $v0, 5
	syscall
	
	sw $v0, array($s2)								
		
	addu $s1, $s1, 1
	bne $s1, $t1, loop2
	
	move $s1, $zero
	addu $s0, $s0, 1
	bne $s0, $t0, loop2
	
	
# the result 
	li $t2, 0
for_1_begin:
	slt $t4, $t2, $t0
	beq $t4, $zero, for_1_end
	
	
	li $t3, 0
for_2_begin:
	slt $t4, $t3, $t1
	beq $t4, $zero, for_2_end
		
	mult $t1, $t2
	mflo $s2
	addu $s2, $s2, $t3
	sll $s2, $s2, 2
	
	lw $t5, data($s2)
	lw $t6, array($s2)
	addu $t5, $t5, $t6
	sw $t5, result($s2)
	
	
	addu $t3, $t3 ,1
	j for_2_begin
for_2_end:	
	
	
	addu $t2, $t2, 1
	j for_1_begin		
for_1_end:	
	
	
	
#the output
output:
	
	li $t2, 0
for_3_begin:
	slt $t4, $t2, $t1
	beq $t4, $zero, for_3_end
				
	li $t3 , 0
for_4_begin:
	slt $t4, $t3, $t0	
	beq $t4, $zero, for_4_end
	

	mult $t3,$t1
	mflo $s2
	addu $s2, $s2, $t2
	sll $s2, $s2, 2
	
	
	lw $a0, result($s2)
	li $v0, 1
	syscall
	
	subi $s6, $t0, 1
if: beq $s6, $t3, else_1
	
	li $v0, 4
	la $a0, space
	syscall
else_1:	
	
	addu $t3, $t3, 1
	j for_4_begin
for_4_end:	
	
			
	subi $s6, $t1, 1
if_2: beq $s6, $t2, else_2
							
	li $v0, 4
	la $a0, enter
	syscall
else_2:	
	
	addu $t2, $t2, 1
	j for_3_begin
for_3_end:	
	
	
	
	
	
	
	
	
											
	#########
#      mult A, B
#       mflo C	
#	A是:
#	最里层的一个循环计数器或者最外层的循环计数器
#   	决定了寻找元素的方式是一行一行还是一列一列
#
#	B是:
#	行数或者是列数
#	决定了寻找元素的间隔
#
#
#	矩阵相关的最重要的就是:对矩阵元素的存取
