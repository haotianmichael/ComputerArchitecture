.data

	array: .space 32
	symbol: .space  32
	pp:.space 64
	space: .asciiz " "
	enter: .asciiz "\n"
	
.text

main:
	li $v0, 5
	syscall 
	move $t0, $v0   #this is the n
	move $a0, $zero   #index
	
	jal FullArray
	
	li $v0, 10
	syscall
	
FullArray:
	sge $t1, $a0, $t0   # if(index >= n)
	beq $t1, $zero, work
	
	li $t2, 0  #the i
	for_1_begin:       
	slt $t1, $t2, $t0
	beq $t1, $zero, for_1_end
	
	li $t1, 4
	la $t3, array
	mult $t1, $t2
	mflo $t1
	add $t3, $t3, $t1
	lw $t4, array($t3)
	
	li $v0,1 
	move $a0, $t4   #printf("%d", array[i]);
	syscall
	
	#sw $a0, 0($sp)
	#subi $sp, $sp, 4
	li $v0, 4
	la $a0, space       #printf(" ");
	syscall
	#addi $sp, $sp, 4
	#lw $a0, 0($sp)
	
	addi $t2, $t2, 1   #printf("\n");
	j for_1_begin
	for_1_end:
	#sw $a0, 0($sp)
	#subi $sp, $sp, 4
	li $v0, 4
	la $a0, enter
	syscall
	#addi $sp, $sp, 4
	#lw $a0, 0($sp)
	
	jr $31
	
work:
	
	li $t2, 0
	for_2_begin:     #for(i=0;i<n;i++)  beignning
	slt $t3, $t2, $t0
	beq $t3, $zero, for_2_end
	
	li $t1, 4    # if(symbol[i] == 0)
	la $t3, symbol
	mult $t1, $t2
	mflo $t1
	add $t3, $t3, $t1
	lw $t4, symbol($t3)
	
	bne $t4, $zero, if_1_else
	
	li $t1, 4
	la $t3, array
	mult $t1, $a0
	mflo $t1
	add $t3, $t3, $t1
	addi $t5, $t2, 1  
	sw $t5, array($t3)   #array[index] = i+1;
	
			
	li $t1, 4
	la $t3, symbol
	mult $t1, $t2
	mflo $t1
	add $t3, $t3, $t1
	addi $t5, $zero, 1
	sw $t5, symbol($t3)   #symbol[i] = 1;
	
	sw $ra, 0($sp)
	subi $sp, $sp, 4
	sw $t2, 0($sp)
	subi $sp, $sp, 4
	sw $a0, 0($sp)
	subi $sp, $sp, 4
	
	
	addi $t5, $a0, 1
	move $a0, $t5
	jal FullArray    #FullArray(index+1);
	
	
	addi $sp, $sp, 4
	lw $a0, 0($sp)
	addi $sp, $sp, 4
	lw $t2, 0($sp)
	addi $sp, $sp, 4
	lw $ra, 0($sp)
	
	li $t1, 4     #symbol[i] = 0;
	la $t3, symbol
	mult $t2, $t1
	mflo $t1
	add $t3, $t3, $t1
	sw $zero, symbol($t3)
	
	
	if_1_else:
	addi $t2, $t2, 1   #for(i=0;i<n;i++)  ending
	j for_2_begin
	for_2_end:
	jr $31
	
	
	


	
