.data
	data: .word 0:64
	array: .word 0:64
	space: .asciiz " "
	enter: .asciiz "\n"

.text
	
input:
	li $v0, 5
	syscall
	move $t0, $v0
	move $t1, $v0	
	
	
#the first matrix 
	li $s0, 0
	li $s1, 0
loop1:
	mult $t1, $s0
	mflo $s2
	addu $s2, $s2, $s1
	sll $s2, $s2, 2
	
	li $v0, 5 
	syscall
			
	sw $v0, data($s2)
	
	addu $s1, $s1, 1
	bne $s1, $t1, loop1
	
	move $s1, $zero
	addu $s0, $s0, 1
	bne $s0, $t0, loop1
	
	
# the second matrix 
	li $s0, 0
	li $s1, 0
loop2:
	mult $t1, $s0
	mflo $s2
	addu $s2, $s2, $s1
	sll $s2, $s2, 2
	
	li $v0, 5
	syscall
	
	sw $v0, array($s2)
	
	addu $s1, $s1, 1
	bne $s1, $t0, loop2
	
	  
	move $s1, $zero
	addu $s0, $s0, 1
	bne $s0, $t0, loop2
	
	
#the result
	li $t1, 0
for_1_begin:
	slt $t4, $t1, $t0
	beq $t4, $zero, for_1_end	
			
	
	
	li $t2, 0
for_2_begin:
	slt $t4, $t2, $t0
	beq $t4, $zero, for_2_end
		
	
	
	li $t3, 0
for_3_begin:	
	slt $t4, $t3, $t0
	beq $t4, $zero, for_3_end
	
	mult $t0, $t1
	mflo $s2
	addu $s2, $s2, $t3
	sll $s2, $s2, 2
	
	lw $t5, data($s2)    # the first matrix
	
	mult $t0, $t3
	mflo $s2
	addu $s2, $s2, $t2
	sll $s2, $s2, 2
	
	lw $t6, array($s2)   # the second matrix
		
	mult $t5, $t6
	mflo $t5
	add $t7, $t7, $t5    #the result of two matrix 
  	
	
	addu $t3, $t3, 1
	j for_3_begin
for_3_end:	
	move $t3, $zero
	
	move $a0, $t7
	li $v0, 1
	syscall
	
	la $a0, space
	li $v0, 4
	syscall		
	
	
	move $t7, $zero
	
	
	addu $t2, $t2, 1
	j for_2_begin
for_2_end:	
	move $t2, $zero
	
	li $v0, 4
	la $a0, enter
	syscall
	
		
	
	addu  $t1, $t1, 1
	j for_1_begin
for_1_end:	
	li $v0, 10
	syscall
	
	
	
	
	
	
	
	
	#########################
#
#	矩阵元素的存取:
#	
#	m行n列
#	$t0  代表 m
#       $t1  代表 n
#	$s0  代表行数的计数器
#	$s1  代表列数的计数器
#
#	一行一行的取元素:
#
#
#	li $s0, 0
#for_1_begin:
#	slt $t2, $s0, $t0
#	beq $t2, $zero, for_1_end
#
#	
#	li $s1, 0
#for_2_begin:	
#	slt $t2, $s1, $t1
#	beq $t2, $zero, for_2_end
#
#	
#         mult $s0, $t1    行计数器乘以列数  确定第几行
#     	  mflo $s2	 
#         addu $s2, $s2, $s1   加上列计数器   （确定该在第几行第几列）
#	  sll $s2, $s2, 2   乘以4
#	  lw $t3, data($s2)
#		
#	addu $s1, $s1, 1
#	j for_2_begin
#for_2_end:
#	move $s0, $zero
#
#
#	addu $s0, $s0, 1
#	j for_1_begin
#for_1_end:	
	
	
