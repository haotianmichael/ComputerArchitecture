.data
	data: .word 0:64
	space: .asciiz " "
	
.text

input:
	li $v0, 5
	syscall
	move $t0, $v0
	
	li $t1, 0
loop:	
	slt $t2, $t1, $t0
	beq $t2, $zero, loop_end
	
	li $v0, 5
	syscall
	
	li $s0, 0
	addu $s0, $s0, $t1
	sll $s0, $s0, 2
	sw $v0, data($s0)	
	
	addu $t1, $t1, 1
	j loop
loop_end:


sort:
	li $t1, 0   #i
for_1_begin:	
	slt $t3, $t1, $t0
	beq $t3, $zero, for_1_end
	
	
	
	move $t4, $t1  #min
	addi $t2, $t1, 1   #j
for_2_begin:
	slt $t3, $t2, $t0
	beq $t3, $zero, for_2_end	
	
	li $s0, 0
	addu $s0, $s0, $t4
	sll $s0, $s0, 2
	lw $t5, data($s0)
	
	li $s1, 0
	addu $s1, $s1, $t2
	sll $s1, $s1, 2
	lw $t6, data($s1)
	
if: slt $t7, $t5, $t6
	bne $t7, $zero, else
		
	
	move $t4, $t2
else:	
	
	addu $t2, $t2, 1
	j for_2_begin
for_2_end:	
	move $t2, $zero
	
	
if_2: beq $t1, $t4, else_2

	li $s0, 0
	addu $s0, $s0, $t1
	sll $s0, $s0, 2
	lw $t8, data($s0)
	
	li $s1, 0
	addu $s1, $s1, $t4
	sll $s1, $s1, 2
	lw $t9, data($s1)
	
	
	sw $t8, data($s1)
	sw $t9, data($s0)
	
else_2:

	addu $t1, $t1, 1
	j for_1_begin
for_1_end:	
	
	
	
output:
	li $t1, 0
for_3_begin:
	slt $t2, $t1, $t0
	beq $t2, $zero, for_3_end
	
			
	li $s0, 0
	addu $s0, $s0, $t1
	sll $s0, $s0, 2
	lw $a0, data($s0)
	li $v0, 1
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
							
	
	
	addu $t1, $t1, 1
	j for_3_begin
for_3_end:	
	
	#####################
#   当元素存取的单位是.word 即以字节为单位进行存取时。对数组元素的存取:
#	.data
#		data: .word 0:64
#	.text
#
#  	li $t1, 0	
#for_1_begin:
#	slt $t2, $t1, $t0
#	beq $t2 ,$zero, for_1_end
#	#	
#	li $s0, 0
#	addu $s0, $s0, $t1     可以看到这个$t1实际上就是一行矩阵（比较矩阵元素的存取#）中的行计数器
#	sll $s0, $s0, 2
#	lw $t4, data($s0)
#
#	addu $t1, $t1, 1
#	j for_1_begin
#for_1_end:
#
#

	
	
