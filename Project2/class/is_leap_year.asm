.data 

is_false: .asciiz "0"
is_true: .asciiz "1"

.text

#input a integer that be moved to register $t0
la $v0, 5
syscall
move $t0, $v0


li $t1 4
li $t2 100
li $t3 400
li $t5 1

divu $t0, $t1   #4
mfhi $t4
slt $t1, $t4, $t5


divu $t0, $t2     #100
mfhi $t4
slt $t2, $t5, $t4

divu $t0, $t3     #400
mfhi $t4
slt $t3, $t4, $t5

and $t6, $t1, $t2
or $t7, $t6, $t3

beq $t7, $zero, if_end 


la $a0, is_true
li $v0, 4
syscall
j if_ended


if_end:
	la $a0, is_false
	li $v0, 4
	syscall

if_ended: