`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:15:19 07/23/2018 
// Design Name: 
// Module Name:    com 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module com(
    input [3:0] A,
    input [3:0] B,
    output reg C
    );
   reg   [3:0] tmp;


   
   always @ (*) begin
/*  in order to change A <= B ==> C = 1;      
 if (A == B) begin
	 C = 0;
      end 
      else*/
	if ({A[3]} == {B[3]}) 
	begin
	   tmp = (~B + 1'b1) + A;
	   if ({tmp[3]} == 0) begin
	      C = 1;  // A >= B
	   end 
	   else begin
	      C = 0;  // A < B
	   end
	end 
      else
	begin
	   if ({A[3]} == 0) begin
	      C = 1;  //A > B
	   end
	   else begin
	      C = 0;  // A < B
	   end
	end


   end
   
   
endmodule
