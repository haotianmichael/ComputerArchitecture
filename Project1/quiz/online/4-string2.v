`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:55:16 07/22/2018 
// Design Name: 
// Module Name:    string 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module string(
    input clk,
    input clr,
    input [7:0] in,
    output reg out = 0
    );


   integer state = 0;
   always @ (posedge clk or posedge clr) begin
      if (clr) 
	begin
	   state <= 0;
	   out <= 0;
	end
      else
	begin
	   case (state) 
	     0: 
	       begin
		  if (in >= "0"  && in <= "9") 
		    begin
		       state <= 1;
		       out <= 1;
		    end
		  else if (in == "(") begin
		     state <= 4;
		     out <= 0;		     
		  end 
		  else if (in == "+" || in == "*") 
		    begin
		       state <= 3;
		       out <= 0;
		    end
	       end
	     1: begin
		if (in == "+" || in == "*") 
		  begin
		     state <= 2;
		     out <= 0;
		  end
		else if (in >= "0"  && in <= "9") 
		  begin
		     state <= 3;
		     out <= 0;
		  end
	     end
	     2: begin
		if (in >= "0" && in <= "9" ) 
		  begin
		     state <= 1;
		     out <= 1;
		  end
		else if (in == "+" || in == "*") 
		  begin
		     state <= 3;
		     out <= 0;		     
		  end 
	     end // case: 2
	     else if (in == "(") begin
		state <= 4;
		out <= 0;
	     end 
	     3:
	       begin
		  out <= 0;
	       end
	     4: 
	       begin
		  if ( in>= "0" && in <= "9") begin
		     state  <= 5;
		     out <= 0;
		  end
		  else begin
		     state <= 3;
		     out <= 0;
		  end
	       end
	     5: 
	       begin
		  if (in == "*" || in == "+") begin
		     state <=  6;
		     out <= 0;
		  end
		  else if (in == ")") begin
		     state <= 7;
		     out <= 1;
		  end
		  else begin
		     state <= 3;
		     out <= 0;		     
		  end
	       end
	     6: 
	       begin
		  if ( n>= "0" && n<= "9") begin
		     state <= 5;
		     out <= 0;
		  end
		  else begin
		     state <= 3;
		     out <= 0;
		  end
	       end
	     7:
	       begin
		  if (in == "*" || in == "+") begin
		     state <= 0;
		     out <= 0;
		  end
		  else begin
		     state <= 3;
		     out <= 0;
		  end
	   endcase // case (etats)
	   
	end
   end
   


   
endmodule
/*
 在P1课下的时候做过一个FSM，在线上的只不过是加上了括号；所以需要在原来的状态机的基础上加上括号的一部分。因为括号涉及到一些输出的问题所以S6和S7状态就是S2和S3状态的Copy；
 */
