`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:34:22 07/21/2018 
// Design Name: 
// Module Name:    alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu(
	   input [31:0]      A,
	   input [31:0]      B,
	   input [3:0] 	     ALUOp,
	   input 	     Clk,
	   output reg [31:0] C
    );

   always @ (*) begin
      case (ALUOp) 
	3'b000: begin
	    C = A  + B;
	end
	3'b001: begin
	   C   = A - B;
	end
	3'b010: begin
	    C  = A & B;
	end
	3'b011: begin
	    C = A | B;
	end
	3'b100: begin
	    C = A >> B;
	end
	3'b101: begin
	   C = $signed(A) >>> B;
	end
	3'b110: begin
	   if (A > B) begin
	      C = 1;
	   end
	   else begin
	      C = 0;
	   end
	end
	3'b111: begin
	   if ($signed(A) > $signed(B)) begin
	      C = 1;
	   end
	   else begin
	      C = 0;
	   end
	end
      endcase // case (pOULA)
   end
      
endmodule
