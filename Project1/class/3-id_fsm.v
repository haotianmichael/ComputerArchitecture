`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:41:30 07/22/2018 
// Design Name: 
// Module Name:    id_fsm 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module id_fsm(
    input [7:0] char,
    input clk,
    output reg out = 0
    );

   integer     state = 0;
   always @ (posedge clk) begin
      case (state) 
	0:
	  begin
	     state <= ((char >= "a" && char <= "z") || (char >= "A" && char <= "Z")) ? 1 : 0;
	     out <= 0;
	  end
	1:
	  begin
	     if (char >= "0" && char <= "9") begin
		state = 2;
	     end
	     else
	       begin
		  if ((char >= "a" && char <= "z") ||(char >= "A" && char <= "Z")) begin
		     state = 1;
		  end	
		  else begin
		     state  = 0;
		  end
	       end
	     out <= (char >= "0" && char <= "9");
	  end
	2:
	  begin
	     if (char >= "0"&& char <= "9") begin
		state = 2;
	     end
	     else begin
		if ((char >= "a" && char <= "z") || (char >= "A" && char <= "Z") ) begin
		   state = 1;
		end
		else begin
		   state = 0;
		end
	     end
	     out  <= (char >= "0" && char <= "9");	     		      
	  end
      endcase      
   end
endmodule
