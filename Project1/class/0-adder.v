`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:21:18 07/18/2018 
// Design Name: 
// Module Name:    Adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Adder(
    input 	clk,
    input 	A,
    input [1:0] en, 
    input 	B,
    output 	Overflow,
    output 	sum
    );
   reg [1:0] C, D;

   assign {Overflow, sum} = C + D;
      
   always @ (posedge clk) begin
      if (en) begin
	 C <= A;
	 D <= B;
      end 
   end
   

endmodule
