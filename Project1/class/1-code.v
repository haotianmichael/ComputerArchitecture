`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:44:21 07/22/2018 
// Design Name: 
// Module Name:    code 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module code(
    input 	      Clk,
    input 	      Reset,
    input 	      Slt,
    input 	      En,
    output reg [63:0] Output0,
    output reg [63:0]     Output1
    );

   reg   [31:0] i;
   initial begin
      i = 0;
      Output0 = 0;
      Output1 = 0;
   end
   
   
   always @ (posedge Clk) begin
      if (Reset) 
	begin
	   Output0 <= 0;
	   Output1 <= 0;	 	 
	end 
      else
	begin
	   if (En) begin
	      if (Slt) 
		begin
		   if (i == 3) begin
		      Output1 <= Output1 + 1'b1;
		      i <= 0;
		   end
		   else begin
		      i <= i + 1'b1;
		   end
		end 
	      else
		begin
		   Output0 <= Output0 + 1'b1;
		end	      
	   end 
	end

      
   end
   



   
endmodule
